#!/bin/dash

[ ${#} -lt 2 ] && printf "\nUsage: $(basename $0) first last [newline]\n
Read lines from stdin and output those in the range [first, last], to
stdout. The optional newline argument specifies the newline character
for the output, the default is '\\\n'. The input ordering is preserved.\n
" >&2 && exit 1

[ "X${1}" = "X." ] && print=true || print=false

while read line; do
  ${print} && printf "${line}${3:-\n}"
  case ${line} in
    ${1}) print=true; printf "${line}${3:-\n}";;
    ${2}) exit 0;;
  esac
done

exit 0
