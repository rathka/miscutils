# miscutils

Collection of useful wrapper scripts.

## SSH

`/usr/bin/ssh-multiadd`: wrapper for ssh-add.
`/etc/profile.d/ssh-agent.sh`: environment settup up ssh-agent

### Note 
To prevent Gnome from overwriting ssh-agent variable,
the variable `GSM_SKIP_SSH_AGENT_WORKAROUND` must be
set to true in `/etc/environment` or `~/.pam_environment`.

Example entry in `/etc/environment`:
`GSM_SKIP_SSH_AGENT_WORKAROUND="true"`

## Lazymount

`/usr/lib/lazymount.sh`: wrapper for udiskctl.
Should be called as `mnt` or `umnt`.

### TODO

Missing support for loop mounts.
