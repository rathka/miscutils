#!/bin/dash

cmd=$(basename $0)

err(){
  printf "${cmd}: $2: $1\n" >&2
  continue
}

blkdev(){
  dev="$(readlink -e $1 || readlink -e /dev/$1)" &&
    [ -b "$dev" ] && printf "${dev}" ||
      err "no such block device" "$1"
}

luksdev(){
  udisksctl info -b $1 | grep -q 'crypto_LUKS' && printf "$1"
}

case ${cmd} in
mnt)
  for arg in $@; do
    dev="$(blkdev ${arg})"
    if luksdev ${dev} >/dev/null; then
      printf "${dev}: "
      arg="$(udisksctl unlock -b ${dev})" || continue
      dev="$(echo ${arg##* } | sed 's|\.$||')"
    fi
    udisksctl mount -b ${dev} || continue
  done;;

lmnt)
  mnt $(for arg in $@; do
    [ -f "${arg}" ] || err "no such file" "${arg}"
    arg="$(udisksctl loop-setup -f ${arg})" || continue
    echo "${arg##* }" | sed 's|\.$||'
  done);;

umnt)
  for arg in $@; do
    unset luks
    if [ -d "${arg}" ]; then
      dir="$(readlink -e ${arg})"
      arg="$(grep "${dir}" /proc/mounts)" ||
        err "invalid mountpoint" "${dir}"
      dev="${arg%% *}"
    else
      dev="$(blkdev ${arg})"
      if luks="$(luksdev ${dev})"; then
        arg="$(udisksctl info -b ${luks} | grep 'IdUUID')" &&
          dev="$(readlink -e "/dev/mapper/luks-${arg##* }")" ||
            err "device not unlocked with udisksctl" "${luks}"
      fi
    fi
    udisksctl unmount -b ${dev} || continue
    [ -b "${luks}" ] && udisksctl lock -b ${luks}
    udisksctl info -b ${dev} | grep -q 'Loop' &&
      udiskctl loop-delete -b ${dev} || continue
  done;;
esac

