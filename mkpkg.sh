#!/bin/sh

cd $(dirname $0)
[ -d '.aur' ] && ( rm -r .aur || exit 1 )
mkdir .aur; cd .aur
find ../*/* -type f -exec ln -s {} ./ \;
pkgbuild=$(mktemp)

cat > ${pkgbuild} << EOF
# Maintainer: Rasmus Thystrup Karstensen <rathka at gmail dot com>
pkgname='rtkutils'
pkgver='0.5'
pkgrel='1'
pkgdesc='Useful scripts and tools'
arch=(any)
license=(as-is)
depends=(dash udisks2)

source=($(find -L -type f))
sha256sums=($(find -L -type f -exec echo "SKIP" \;))

package() {
  install -d  \${pkgdir}/usr/bin
  install -T -D -m 755 lazymount.sh \${pkgdir}/usr/lib/lazymount
  for e in mnt umnt; do
    ln -s ../lib/lazymount \${pkgdir}/usr/bin/\${e}
  done
  #install -T -D -m 755 strange.sh \${pkgdir}/usr/bin/strange
  #install -T -D -m 755 sanitize.sh \${pkgdir}/usr/bin/sanitize.sh
}
EOF

mv ${pkgbuild} PKGBUILD
updpkgsums
tar chf rtkutils.tar.xz *
makepkg $@
